<?php

namespace Internship\Helloworld\Controller\Index;

use Internship\Helloworld\Helper\Data;
use Magento\Framework\App\Action\Action;

class Config extends Action
{
    protected $helperData;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        Data $helperData
    ) {
        $this->helperData = $helperData;
        return parent::__construct($context);
    }

    public function execute()
    {

        // TODO: Implement execute() method.

        echo $this->helperData->getGeneralConfig('enable');
        echo $this->helperData->getGeneralConfig('display_text');
        exit();
    }
}
