<?php

namespace Internship\Helloworld\Plugin;

use Internship\Helloworld\Helper\Data;
use Magento\Framework\Message\Manager;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Message\MessageInterface;

class Notification
{
    protected $_manager;
    protected $helperData;

    /**
     * Message constructor.
     *
     * @param ManagerInterface $manager
     * @param Data $helperData
     */
    public function __construct(
        ManagerInterface $manager,
        Data $helperData
    )
    {
        $this->_manager = $manager;
        $this->helperData = $helperData;
    }

    /**
     * @param Manager $subject
     * @param $text
     * @return mixed
     */
    public function beforeAddSuccessMessage(Manager $message, $text)
    {
        $value = $this->helperData->getGeneralConfig('display_text');
        return $value . '  ' . $text;
    }

    /**
     * @param Manager $message
     * @param array $notification
     * @return int
     */
//    public function beforeAddComplexSuccessMessage(Manager $manager, $notification)
//    {
//        $value = $this->helperData->getGeneralConfig('display_text');
//        return array_push($notification, $value);
//    }

}
