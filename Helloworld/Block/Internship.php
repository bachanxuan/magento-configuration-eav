<?php

namespace Internship\Helloworld\Block;

use Internship\Helloworld\Api\InternshipRepositoryInterface;
use Magento\Framework\View\Element\Template;

class Internship extends Template
{
    protected $_internshipFactory;
    protected $_internshipRepo;

    /**
     * Internship constructor.
     * @param Template\Context $context
     * @param InternshipFactory $internshipFactory
     * @param InternshipRepositoryInterface $internshipRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Internship\Helloworld\Model\InternshipFactory $internshipFactory,
        InternshipRepositoryInterface $internshipRepository,
        array $data = []
    ) {
        $this->_internshipRepo = $internshipRepository;
        $this->_internshipFactory = $internshipFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return Template
     */
    public function _prepareLayout()
    {
        return $this->_internshipRepo->getById(1);
//        return $this->_internshipFactory->create()->load(1);
    }
}
