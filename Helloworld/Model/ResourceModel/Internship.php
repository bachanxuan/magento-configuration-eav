<?php

namespace Internship\Helloworld\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Internship extends AbstractDb
{
    /**
     * Custom method
     */
    public function _construct()
    {
        $this->_init('admin_user', 'user_id');
    }
}
