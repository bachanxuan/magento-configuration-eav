<?php

namespace Internship\Helloworld\Model\ResourceModel\Internship;

use Internship\Helloworld\Model\Internship;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Custom method
     */
    protected function _construct()
    {
        $this->_init(
            \Internship\Helloworld\Model\Internship::class,
            \Internship\Helloworld\Model\ResourceModel\Internship::class
        );
    }
}
