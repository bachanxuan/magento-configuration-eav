<?php

namespace Internship\Helloworld\Model;

use Internship\Helloworld\Api\InternshipRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class InternshipRepository implements InternshipRepositoryInterface
{
    protected $resource;
    protected $internshipFactory;
//    protected $internshipCollectionFactory;

    /**
     * InternshipRepository constructor.
     * @param \Internship\Helloworld\Model\ResourceModel\Internship $resource
     * @param InternshipFactory $blockFactory
     */
    public function __construct(
        \Internship\Helloworld\Model\ResourceModel\Internship $resource,
        InternshipFactory $blockFactory
    ) {
        $this->resource = $resource;
        $this->internshipFactory = $blockFactory;
    }

    /**
     * @inheritDoc
     */
    public function getById($InternshipId)
    {
        $block = $this->internshipFactory->create();
        $this->resource->load($block, $InternshipId);
        if (!$block->getId()) {
            throw new NoSuchEntityException(__('The CMS block with the "%1" ID doesn\'t exist.', $InternshipId));
        }
        return $block;
    }
}
