<?php

namespace Internship\Helloworld\Model;

use Internship\Helloworld\Api\Data\InternshipInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Internship extends AbstractModel implements InternshipInterface
{
    const CACHE_TAG = 'admin_user';
    protected $_cacheTag = self::CACHE_TAG;
    protected $_eventPrefix = self::CACHE_TAG;

    /**
     * Construct
     */
    public function _construct()
    {
        $this->_init(\Internship\Helloworld\Model\ResourceModel\Internship::class);
    }

    /**
     * @inheritDoc
     */
//    public function getIdentities()
//    {
//        return [self::CACHE_TAG . '_' . $this->getId()];
//    }

    /**
     * @inheritDoc
     */
    public function getAdminUsername()
    {
        return $this->getData('username');
    }

}
