<?php

namespace Internship\Helloworld\Api\Data;

interface InternshipInterface
{
    /**
     * Get user name
     *
     * @return string|null
     */
    public function getAdminUsername();

}
