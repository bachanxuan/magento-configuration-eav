<?php

namespace Internship\Helloworld\Api;

use Internship\Helloworld\Api\Data\InternshipInterface;
use Magento\Framework\Exception\LocalizedException;

interface InternshipRepositoryInterface
{
    /**
     * Retrieve faq
     *
     * @param int $InternshipId
     * @return InternshipInterface
     * @throws LocalizedException
     */
    public function getById($InternshipId);

}
