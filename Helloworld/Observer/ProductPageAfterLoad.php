<?php

namespace Internship\Helloworld\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProductPageAfterLoad implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $price = 10;
        $item = $observer->getEvent()->getData('product');
        $item->setPrice($price);
    }
}
