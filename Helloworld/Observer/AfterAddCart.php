<?php

namespace Internship\Helloworld\Observer;

use Internship\Helloworld\Helper\Data;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;

class AfterAddCart implements ObserverInterface
{
    /** @var ManagerInterface */
    protected $messageManager;
    protected $helperData;

    /** @var UrlInterface */
    protected $url;

    /**
     * AfterAddCart constructor.
     * @param ManagerInterface $managerInterface
     * @param UrlInterface $url
     * @param Data $helperData
     */
    public function __construct(
        ManagerInterface $managerInterface,
        UrlInterface $url,
        Data $helperData
    ) {
        $this->messageManager = $managerInterface;
        $this->url = $url;
        $this->helperData = $helperData;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $messageCollection = $this->messageManager->getMessages(true);
        $cartLink = '<a href="' . $this->url->getUrl('checkout/cart') . '">View Cart/Checkout</a>';
        $value = $this->helperData->getGeneralConfig('display_text');
        $this->messageManager->addSuccess($messageCollection->getLastAddedMessage()->getText() . '  ' . $value . ' ' . $cartLink);
    }
}
