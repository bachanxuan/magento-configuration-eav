<?php

namespace Internship\Helloworld\Observer;

use Magento\Framework\Event\ObserverInterface;

class CatalogPageAfterLoad implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $price = 10;
        $items = $observer->getEvent()->getCollection();
        foreach ($items as $item) {
            $item->setPrice($price);
        }
    }
}
